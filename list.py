thislist = ["apple", "banana", "cherry"]
print(thislist)    #displaying the list

print(type(thislist))  #displaying the type
print(len(thislist))   #displaying the length of list in integer format
#accessing with positive index forward order is 0,1,2,3,4,............
#accessing with negative index backward order is -1,-2,-3,.............
print(thislist[-1])


#Checking  if "apple" is present in the list:
thislist = ["apple", "banana", "cherry"]
if "apple" in thislist:
  print("Yes, 'apple' is in the fruits list")

#append refers it places the item in an end
thislist.append("orange")
print(thislist)

# insert refers it places an item in customized position as required.
thislist.insert(1, "orange")
print(thislist)

#Add the elements of tropical to thislist:
tropical = ["mango", "pineapple", "papaya"]
thislist.extend(tropical)
print(thislist)

#removing the elements in a list
thislist = ["apple", "banana", "cherry"]
thislist.remove("banana")
print(thislist)

#removes the element with the help of pop by index,
#  if we dont specify index then it removes the last element as by default
thislist = ["apple", "banana", "cherry"]
thislist.pop(1)
print(thislist)

thislist = ["apple", "banana", "cherry"]
for x in thislist:
  print(x)

# if 'a' letter contains in a list then we can access separately .
fruits = ["apple", "banana", "cherry", "kiwi", "mango"]
newlist = []

for x in fruits:
  if "a" in x:
    newlist.append(x)

print(newlist)

#sorting the elements in a list,ascending by default
thislist1 = ["orange", "mango", "kiwi", "pineapple", "banana"]
thislist1.sort()
print(thislist1)

#descending sort
thislist = ["orange", "mango", "kiwi", "pineapple", "banana"]
thislist.sort(reverse = True)
print(thislist)

